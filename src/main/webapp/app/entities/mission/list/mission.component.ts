import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMission } from '../mission.model';
import { MissionService } from '../service/mission.service';
import { MissionDeleteDialogComponent } from '../delete/mission-delete-dialog.component';

@Component({
  selector: 'jhi-mission',
  templateUrl: './mission.component.html',
})
export class MissionComponent implements OnInit {
  missions?: IMission[];
  isLoading = false;

  constructor(protected missionService: MissionService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.missionService.query().subscribe(
      (res: HttpResponse<IMission[]>) => {
        this.isLoading = false;
        this.missions = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMission): number {
    return item.id!;
  }

  delete(mission: IMission): void {
    const modalRef = this.modalService.open(MissionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.mission = mission;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
