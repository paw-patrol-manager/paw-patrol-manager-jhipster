import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { MissionService } from '../service/mission.service';

import { MissionComponent } from './mission.component';

describe('Mission Management Component', () => {
  let comp: MissionComponent;
  let fixture: ComponentFixture<MissionComponent>;
  let service: MissionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MissionComponent],
    })
      .overrideTemplate(MissionComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MissionComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(MissionService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.missions?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
