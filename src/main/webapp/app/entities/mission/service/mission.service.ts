import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMission, getMissionIdentifier } from '../mission.model';

export type EntityResponseType = HttpResponse<IMission>;
export type EntityArrayResponseType = HttpResponse<IMission[]>;

@Injectable({ providedIn: 'root' })
export class MissionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/missions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(mission: IMission): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mission);
    return this.http
      .post<IMission>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(mission: IMission): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mission);
    return this.http
      .put<IMission>(`${this.resourceUrl}/${getMissionIdentifier(mission) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(mission: IMission): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mission);
    return this.http
      .patch<IMission>(`${this.resourceUrl}/${getMissionIdentifier(mission) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMission>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMission[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMissionToCollectionIfMissing(missionCollection: IMission[], ...missionsToCheck: (IMission | null | undefined)[]): IMission[] {
    const missions: IMission[] = missionsToCheck.filter(isPresent);
    if (missions.length > 0) {
      const missionCollectionIdentifiers = missionCollection.map(missionItem => getMissionIdentifier(missionItem)!);
      const missionsToAdd = missions.filter(missionItem => {
        const missionIdentifier = getMissionIdentifier(missionItem);
        if (missionIdentifier == null || missionCollectionIdentifiers.includes(missionIdentifier)) {
          return false;
        }
        missionCollectionIdentifiers.push(missionIdentifier);
        return true;
      });
      return [...missionsToAdd, ...missionCollection];
    }
    return missionCollection;
  }

  protected convertDateFromClient(mission: IMission): IMission {
    return Object.assign({}, mission, {
      date: mission.date?.isValid() ? mission.date.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((mission: IMission) => {
        mission.date = mission.date ? dayjs(mission.date) : undefined;
      });
    }
    return res;
  }
}
