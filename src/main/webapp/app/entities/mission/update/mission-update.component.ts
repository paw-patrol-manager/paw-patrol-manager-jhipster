import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMission, Mission } from '../mission.model';
import { MissionService } from '../service/mission.service';
import { IPlace } from 'app/entities/place/place.model';
import { PlaceService } from 'app/entities/place/service/place.service';
import { IClient } from 'app/entities/client/client.model';
import { ClientService } from 'app/entities/client/service/client.service';
import { IDog } from 'app/entities/dog/dog.model';
import { DogService } from 'app/entities/dog/service/dog.service';

@Component({
  selector: 'jhi-mission-update',
  templateUrl: './mission-update.component.html',
})
export class MissionUpdateComponent implements OnInit {
  isSaving = false;

  placesSharedCollection: IPlace[] = [];
  clientsSharedCollection: IClient[] = [];
  dogsSharedCollection: IDog[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    date: [null, [Validators.required]],
    description: [],
    missionLocation: [],
    victims: [],
    saviors: [null, Validators.required],
  });

  constructor(
    protected missionService: MissionService,
    protected placeService: PlaceService,
    protected clientService: ClientService,
    protected dogService: DogService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mission }) => {
      this.updateForm(mission);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const mission = this.createFromForm();
    if (mission.id !== undefined) {
      this.subscribeToSaveResponse(this.missionService.update(mission));
    } else {
      this.subscribeToSaveResponse(this.missionService.create(mission));
    }
  }

  trackPlaceById(index: number, item: IPlace): number {
    return item.id!;
  }

  trackClientById(index: number, item: IClient): number {
    return item.id!;
  }

  trackDogById(index: number, item: IDog): number {
    return item.id!;
  }

  getSelectedClient(option: IClient, selectedVals?: IClient[]): IClient {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  getSelectedDog(option: IDog, selectedVals?: IDog[]): IDog {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMission>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(mission: IMission): void {
    this.editForm.patchValue({
      id: mission.id,
      name: mission.name,
      date: mission.date,
      description: mission.description,
      missionLocation: mission.missionLocation,
      victims: mission.victims,
      saviors: mission.saviors,
    });

    this.placesSharedCollection = this.placeService.addPlaceToCollectionIfMissing(this.placesSharedCollection, mission.missionLocation);
    this.clientsSharedCollection = this.clientService.addClientToCollectionIfMissing(
      this.clientsSharedCollection,
      ...(mission.victims ?? [])
    );
    this.dogsSharedCollection = this.dogService.addDogToCollectionIfMissing(this.dogsSharedCollection, ...(mission.saviors ?? []));
  }

  protected loadRelationshipsOptions(): void {
    this.placeService
      .query()
      .pipe(map((res: HttpResponse<IPlace[]>) => res.body ?? []))
      .pipe(map((places: IPlace[]) => this.placeService.addPlaceToCollectionIfMissing(places, this.editForm.get('missionLocation')!.value)))
      .subscribe((places: IPlace[]) => (this.placesSharedCollection = places));

    this.clientService
      .query()
      .pipe(map((res: HttpResponse<IClient[]>) => res.body ?? []))
      .pipe(
        map((clients: IClient[]) =>
          this.clientService.addClientToCollectionIfMissing(clients, ...(this.editForm.get('victims')!.value ?? []))
        )
      )
      .subscribe((clients: IClient[]) => (this.clientsSharedCollection = clients));

    this.dogService
      .query()
      .pipe(map((res: HttpResponse<IDog[]>) => res.body ?? []))
      .pipe(map((dogs: IDog[]) => this.dogService.addDogToCollectionIfMissing(dogs, ...(this.editForm.get('saviors')!.value ?? []))))
      .subscribe((dogs: IDog[]) => (this.dogsSharedCollection = dogs));
  }

  protected createFromForm(): IMission {
    return {
      ...new Mission(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      date: this.editForm.get(['date'])!.value,
      description: this.editForm.get(['description'])!.value,
      missionLocation: this.editForm.get(['missionLocation'])!.value,
      victims: this.editForm.get(['victims'])!.value,
      saviors: this.editForm.get(['saviors'])!.value,
    };
  }
}
