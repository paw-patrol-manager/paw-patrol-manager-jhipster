jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MissionService } from '../service/mission.service';
import { IMission, Mission } from '../mission.model';
import { IPlace } from 'app/entities/place/place.model';
import { PlaceService } from 'app/entities/place/service/place.service';
import { IClient } from 'app/entities/client/client.model';
import { ClientService } from 'app/entities/client/service/client.service';
import { IDog } from 'app/entities/dog/dog.model';
import { DogService } from 'app/entities/dog/service/dog.service';

import { MissionUpdateComponent } from './mission-update.component';

describe('Mission Management Update Component', () => {
  let comp: MissionUpdateComponent;
  let fixture: ComponentFixture<MissionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let missionService: MissionService;
  let placeService: PlaceService;
  let clientService: ClientService;
  let dogService: DogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MissionUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(MissionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MissionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    missionService = TestBed.inject(MissionService);
    placeService = TestBed.inject(PlaceService);
    clientService = TestBed.inject(ClientService);
    dogService = TestBed.inject(DogService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Place query and add missing value', () => {
      const mission: IMission = { id: 456 };
      const missionLocation: IPlace = { id: 81835 };
      mission.missionLocation = missionLocation;

      const placeCollection: IPlace[] = [{ id: 89179 }];
      jest.spyOn(placeService, 'query').mockReturnValue(of(new HttpResponse({ body: placeCollection })));
      const additionalPlaces = [missionLocation];
      const expectedCollection: IPlace[] = [...additionalPlaces, ...placeCollection];
      jest.spyOn(placeService, 'addPlaceToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mission });
      comp.ngOnInit();

      expect(placeService.query).toHaveBeenCalled();
      expect(placeService.addPlaceToCollectionIfMissing).toHaveBeenCalledWith(placeCollection, ...additionalPlaces);
      expect(comp.placesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Client query and add missing value', () => {
      const mission: IMission = { id: 456 };
      const victims: IClient[] = [{ id: 58777 }];
      mission.victims = victims;

      const clientCollection: IClient[] = [{ id: 23105 }];
      jest.spyOn(clientService, 'query').mockReturnValue(of(new HttpResponse({ body: clientCollection })));
      const additionalClients = [...victims];
      const expectedCollection: IClient[] = [...additionalClients, ...clientCollection];
      jest.spyOn(clientService, 'addClientToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mission });
      comp.ngOnInit();

      expect(clientService.query).toHaveBeenCalled();
      expect(clientService.addClientToCollectionIfMissing).toHaveBeenCalledWith(clientCollection, ...additionalClients);
      expect(comp.clientsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Dog query and add missing value', () => {
      const mission: IMission = { id: 456 };
      const saviors: IDog[] = [{ id: 92205 }];
      mission.saviors = saviors;

      const dogCollection: IDog[] = [{ id: 59551 }];
      jest.spyOn(dogService, 'query').mockReturnValue(of(new HttpResponse({ body: dogCollection })));
      const additionalDogs = [...saviors];
      const expectedCollection: IDog[] = [...additionalDogs, ...dogCollection];
      jest.spyOn(dogService, 'addDogToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mission });
      comp.ngOnInit();

      expect(dogService.query).toHaveBeenCalled();
      expect(dogService.addDogToCollectionIfMissing).toHaveBeenCalledWith(dogCollection, ...additionalDogs);
      expect(comp.dogsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const mission: IMission = { id: 456 };
      const missionLocation: IPlace = { id: 9519 };
      mission.missionLocation = missionLocation;
      const victims: IClient = { id: 16802 };
      mission.victims = [victims];
      const saviors: IDog = { id: 78271 };
      mission.saviors = [saviors];

      activatedRoute.data = of({ mission });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(mission));
      expect(comp.placesSharedCollection).toContain(missionLocation);
      expect(comp.clientsSharedCollection).toContain(victims);
      expect(comp.dogsSharedCollection).toContain(saviors);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Mission>>();
      const mission = { id: 123 };
      jest.spyOn(missionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mission });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: mission }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(missionService.update).toHaveBeenCalledWith(mission);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Mission>>();
      const mission = new Mission();
      jest.spyOn(missionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mission });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: mission }));
      saveSubject.complete();

      // THEN
      expect(missionService.create).toHaveBeenCalledWith(mission);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Mission>>();
      const mission = { id: 123 };
      jest.spyOn(missionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mission });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(missionService.update).toHaveBeenCalledWith(mission);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPlaceById', () => {
      it('Should return tracked Place primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPlaceById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackClientById', () => {
      it('Should return tracked Client primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackClientById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackDogById', () => {
      it('Should return tracked Dog primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackDogById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedClient', () => {
      it('Should return option if no Client is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedClient(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Client for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedClient(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Client is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedClient(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });

    describe('getSelectedDog', () => {
      it('Should return option if no Dog is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedDog(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Dog for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedDog(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Dog is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedDog(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
