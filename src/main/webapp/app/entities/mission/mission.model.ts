import * as dayjs from 'dayjs';
import { IPlace } from 'app/entities/place/place.model';
import { IClient } from 'app/entities/client/client.model';
import { IDog } from 'app/entities/dog/dog.model';

export interface IMission {
  id?: number;
  name?: string | null;
  date?: dayjs.Dayjs;
  description?: string | null;
  missionLocation?: IPlace | null;
  victims?: IClient[] | null;
  saviors?: IDog[];
}

export class Mission implements IMission {
  constructor(
    public id?: number,
    public name?: string | null,
    public date?: dayjs.Dayjs,
    public description?: string | null,
    public missionLocation?: IPlace | null,
    public victims?: IClient[] | null,
    public saviors?: IDog[]
  ) {}
}

export function getMissionIdentifier(mission: IMission): number | undefined {
  return mission.id;
}
