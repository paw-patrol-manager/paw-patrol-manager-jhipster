import { IVehicle } from 'app/entities/vehicle/vehicle.model';
import { IMission } from 'app/entities/mission/mission.model';

export interface IDog {
  id?: number;
  name?: string;
  speciality?: string;
  breed?: string;
  vehicles?: IVehicle[] | null;
  missions?: IMission[] | null;
}

export class Dog implements IDog {
  constructor(
    public id?: number,
    public name?: string,
    public speciality?: string,
    public breed?: string,
    public vehicles?: IVehicle[] | null,
    public missions?: IMission[] | null
  ) {}
}

export function getDogIdentifier(dog: IDog): number | undefined {
  return dog.id;
}
