import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDog, Dog } from '../dog.model';

import { DogService } from './dog.service';

describe('Dog Service', () => {
  let service: DogService;
  let httpMock: HttpTestingController;
  let elemDefault: IDog;
  let expectedResult: IDog | IDog[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DogService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      speciality: 'AAAAAAA',
      breed: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Dog', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Dog()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Dog', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          speciality: 'BBBBBB',
          breed: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Dog', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
          breed: 'BBBBBB',
        },
        new Dog()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Dog', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          speciality: 'BBBBBB',
          breed: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Dog', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addDogToCollectionIfMissing', () => {
      it('should add a Dog to an empty array', () => {
        const dog: IDog = { id: 123 };
        expectedResult = service.addDogToCollectionIfMissing([], dog);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(dog);
      });

      it('should not add a Dog to an array that contains it', () => {
        const dog: IDog = { id: 123 };
        const dogCollection: IDog[] = [
          {
            ...dog,
          },
          { id: 456 },
        ];
        expectedResult = service.addDogToCollectionIfMissing(dogCollection, dog);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Dog to an array that doesn't contain it", () => {
        const dog: IDog = { id: 123 };
        const dogCollection: IDog[] = [{ id: 456 }];
        expectedResult = service.addDogToCollectionIfMissing(dogCollection, dog);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(dog);
      });

      it('should add only unique Dog to an array', () => {
        const dogArray: IDog[] = [{ id: 123 }, { id: 456 }, { id: 88995 }];
        const dogCollection: IDog[] = [{ id: 123 }];
        expectedResult = service.addDogToCollectionIfMissing(dogCollection, ...dogArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const dog: IDog = { id: 123 };
        const dog2: IDog = { id: 456 };
        expectedResult = service.addDogToCollectionIfMissing([], dog, dog2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(dog);
        expect(expectedResult).toContain(dog2);
      });

      it('should accept null and undefined values', () => {
        const dog: IDog = { id: 123 };
        expectedResult = service.addDogToCollectionIfMissing([], null, dog, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(dog);
      });

      it('should return initial array if no Dog is added', () => {
        const dogCollection: IDog[] = [{ id: 123 }];
        expectedResult = service.addDogToCollectionIfMissing(dogCollection, undefined, null);
        expect(expectedResult).toEqual(dogCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
