import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDog, getDogIdentifier } from '../dog.model';

export type EntityResponseType = HttpResponse<IDog>;
export type EntityArrayResponseType = HttpResponse<IDog[]>;

@Injectable({ providedIn: 'root' })
export class DogService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/dogs');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(dog: IDog): Observable<EntityResponseType> {
    return this.http.post<IDog>(this.resourceUrl, dog, { observe: 'response' });
  }

  update(dog: IDog): Observable<EntityResponseType> {
    return this.http.put<IDog>(`${this.resourceUrl}/${getDogIdentifier(dog) as number}`, dog, { observe: 'response' });
  }

  partialUpdate(dog: IDog): Observable<EntityResponseType> {
    return this.http.patch<IDog>(`${this.resourceUrl}/${getDogIdentifier(dog) as number}`, dog, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDog>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDog[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDogToCollectionIfMissing(dogCollection: IDog[], ...dogsToCheck: (IDog | null | undefined)[]): IDog[] {
    const dogs: IDog[] = dogsToCheck.filter(isPresent);
    if (dogs.length > 0) {
      const dogCollectionIdentifiers = dogCollection.map(dogItem => getDogIdentifier(dogItem)!);
      const dogsToAdd = dogs.filter(dogItem => {
        const dogIdentifier = getDogIdentifier(dogItem);
        if (dogIdentifier == null || dogCollectionIdentifiers.includes(dogIdentifier)) {
          return false;
        }
        dogCollectionIdentifiers.push(dogIdentifier);
        return true;
      });
      return [...dogsToAdd, ...dogCollection];
    }
    return dogCollection;
  }
}
