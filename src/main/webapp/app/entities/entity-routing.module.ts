import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'dog',
        data: { pageTitle: 'pawpatrolmanagerApp.dog.home.title' },
        loadChildren: () => import('./dog/dog.module').then(m => m.DogModule),
      },
      {
        path: 'client',
        data: { pageTitle: 'pawpatrolmanagerApp.client.home.title' },
        loadChildren: () => import('./client/client.module').then(m => m.ClientModule),
      },
      {
        path: 'place',
        data: { pageTitle: 'pawpatrolmanagerApp.place.home.title' },
        loadChildren: () => import('./place/place.module').then(m => m.PlaceModule),
      },
      {
        path: 'mission',
        data: { pageTitle: 'pawpatrolmanagerApp.mission.home.title' },
        loadChildren: () => import('./mission/mission.module').then(m => m.MissionModule),
      },
      {
        path: 'vehicle',
        data: { pageTitle: 'pawpatrolmanagerApp.vehicle.home.title' },
        loadChildren: () => import('./vehicle/vehicle.module').then(m => m.VehicleModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
