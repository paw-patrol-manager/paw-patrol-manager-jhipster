import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { PlaceService } from '../service/place.service';

import { PlaceComponent } from './place.component';

describe('Place Management Component', () => {
  let comp: PlaceComponent;
  let fixture: ComponentFixture<PlaceComponent>;
  let service: PlaceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PlaceComponent],
    })
      .overrideTemplate(PlaceComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PlaceComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PlaceService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.places?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
