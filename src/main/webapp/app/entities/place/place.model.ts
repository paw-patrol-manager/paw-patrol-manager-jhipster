import { IMission } from 'app/entities/mission/mission.model';

export interface IPlace {
  id?: number;
  name?: string;
  missions?: IMission[] | null;
}

export class Place implements IPlace {
  constructor(public id?: number, public name?: string, public missions?: IMission[] | null) {}
}

export function getPlaceIdentifier(place: IPlace): number | undefined {
  return place.id;
}
