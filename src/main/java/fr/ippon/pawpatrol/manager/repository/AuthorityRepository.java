package fr.ippon.pawpatrol.manager.repository;

import fr.ippon.pawpatrol.manager.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {}
