package fr.ippon.pawpatrol.manager.repository;

import fr.ippon.pawpatrol.manager.domain.Place;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Place entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlaceRepository extends JpaRepository<Place, Long> {}
