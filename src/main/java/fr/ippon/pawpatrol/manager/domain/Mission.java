package fr.ippon.pawpatrol.manager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Mission.
 */
@Entity
@Table(name = "mission")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Mission implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties(value = { "missions" }, allowSetters = true)
    private Place missionLocation;

    @ManyToMany
    @JoinTable(
        name = "rel_mission__victims",
        joinColumns = @JoinColumn(name = "mission_id"),
        inverseJoinColumns = @JoinColumn(name = "victims_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "savings" }, allowSetters = true)
    private Set<Client> victims = new HashSet<>();

    @ManyToMany
    @NotNull
    @JoinTable(
        name = "rel_mission__saviors",
        joinColumns = @JoinColumn(name = "mission_id"),
        inverseJoinColumns = @JoinColumn(name = "saviors_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "vehicles", "missions" }, allowSetters = true)
    private Set<Dog> saviors = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Mission id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Mission name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Mission date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return this.description;
    }

    public Mission description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Place getMissionLocation() {
        return this.missionLocation;
    }

    public void setMissionLocation(Place place) {
        this.missionLocation = place;
    }

    public Mission missionLocation(Place place) {
        this.setMissionLocation(place);
        return this;
    }

    public Set<Client> getVictims() {
        return this.victims;
    }

    public void setVictims(Set<Client> clients) {
        this.victims = clients;
    }

    public Mission victims(Set<Client> clients) {
        this.setVictims(clients);
        return this;
    }

    public Mission addVictims(Client client) {
        this.victims.add(client);
        client.getSavings().add(this);
        return this;
    }

    public Mission removeVictims(Client client) {
        this.victims.remove(client);
        client.getSavings().remove(this);
        return this;
    }

    public Set<Dog> getSaviors() {
        return this.saviors;
    }

    public void setSaviors(Set<Dog> dogs) {
        this.saviors = dogs;
    }

    public Mission saviors(Set<Dog> dogs) {
        this.setSaviors(dogs);
        return this;
    }

    public Mission addSaviors(Dog dog) {
        this.saviors.add(dog);
        dog.getMissions().add(this);
        return this;
    }

    public Mission removeSaviors(Dog dog) {
        this.saviors.remove(dog);
        dog.getMissions().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Mission)) {
            return false;
        }
        return id != null && id.equals(((Mission) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Mission{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", date='" + getDate() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
