package fr.ippon.pawpatrol.manager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Dog.
 */
@Entity
@Table(name = "dog")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Dog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "speciality", nullable = false)
    private String speciality;

    @NotNull
    @Column(name = "breed", nullable = false)
    private String breed;

    @OneToMany(mappedBy = "owner")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "owner" }, allowSetters = true)
    private Set<Vehicle> vehicles = new HashSet<>();

    @ManyToMany(mappedBy = "saviors")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "missionLocation", "victims", "saviors" }, allowSetters = true)
    private Set<Mission> missions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Dog id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Dog name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpeciality() {
        return this.speciality;
    }

    public Dog speciality(String speciality) {
        this.setSpeciality(speciality);
        return this;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getBreed() {
        return this.breed;
    }

    public Dog breed(String breed) {
        this.setBreed(breed);
        return this;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Set<Vehicle> getVehicles() {
        return this.vehicles;
    }

    public void setVehicles(Set<Vehicle> vehicles) {
        if (this.vehicles != null) {
            this.vehicles.forEach(i -> i.setOwner(null));
        }
        if (vehicles != null) {
            vehicles.forEach(i -> i.setOwner(this));
        }
        this.vehicles = vehicles;
    }

    public Dog vehicles(Set<Vehicle> vehicles) {
        this.setVehicles(vehicles);
        return this;
    }

    public Dog addVehicles(Vehicle vehicle) {
        this.vehicles.add(vehicle);
        vehicle.setOwner(this);
        return this;
    }

    public Dog removeVehicles(Vehicle vehicle) {
        this.vehicles.remove(vehicle);
        vehicle.setOwner(null);
        return this;
    }

    public Set<Mission> getMissions() {
        return this.missions;
    }

    public void setMissions(Set<Mission> missions) {
        if (this.missions != null) {
            this.missions.forEach(i -> i.removeSaviors(this));
        }
        if (missions != null) {
            missions.forEach(i -> i.addSaviors(this));
        }
        this.missions = missions;
    }

    public Dog missions(Set<Mission> missions) {
        this.setMissions(missions);
        return this;
    }

    public Dog addMissions(Mission mission) {
        this.missions.add(mission);
        mission.getSaviors().add(this);
        return this;
    }

    public Dog removeMissions(Mission mission) {
        this.missions.remove(mission);
        mission.getSaviors().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Dog)) {
            return false;
        }
        return id != null && id.equals(((Dog) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Dog{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", speciality='" + getSpeciality() + "'" +
            ", breed='" + getBreed() + "'" +
            "}";
    }
}
