package fr.ippon.pawpatrol.manager.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "species", nullable = false)
    private String species;

    @Column(name = "occupation")
    private String occupation;

    @Column(name = "description")
    private String description;

    @ManyToMany(mappedBy = "victims")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "missionLocation", "victims", "saviors" }, allowSetters = true)
    private Set<Mission> savings = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Client id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Client name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return this.species;
    }

    public Client species(String species) {
        this.setSpecies(species);
        return this;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getOccupation() {
        return this.occupation;
    }

    public Client occupation(String occupation) {
        this.setOccupation(occupation);
        return this;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getDescription() {
        return this.description;
    }

    public Client description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Mission> getSavings() {
        return this.savings;
    }

    public void setSavings(Set<Mission> missions) {
        if (this.savings != null) {
            this.savings.forEach(i -> i.removeVictims(this));
        }
        if (missions != null) {
            missions.forEach(i -> i.addVictims(this));
        }
        this.savings = missions;
    }

    public Client savings(Set<Mission> missions) {
        this.setSavings(missions);
        return this;
    }

    public Client addSavings(Mission mission) {
        this.savings.add(mission);
        mission.getVictims().add(this);
        return this;
    }

    public Client removeSavings(Mission mission) {
        this.savings.remove(mission);
        mission.getVictims().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", species='" + getSpecies() + "'" +
            ", occupation='" + getOccupation() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
