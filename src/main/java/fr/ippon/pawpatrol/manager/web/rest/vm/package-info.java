/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.ippon.pawpatrol.manager.web.rest.vm;
